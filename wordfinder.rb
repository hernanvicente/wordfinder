require 'bundler/setup'
require 'sinatra'
require 'sinatra/reloader' if development?
require 'slim'


class WordFinder
  attr_reader :grid, :dictionary, :founded_words
  attr_writer :grid, :dictionary, :founded_words

  def initialize
    @grid           = Array.new
    @dictionary     = "Empty"
    @founded_words  = Array.new
  end

  def generate_row(n)
    row = Array.new
    n.times do
      row << ('A'..'Z').to_a.sample
    end
    return row
  end

  def generate_grid(n)
    n.times {grid << generate_row(n)}
    return grid
  end

  def load_dictionary
    lines_with_index= []
    File.readlines('lib/dict.txt').each do |line|
      lines_with_index << (line.split(" ", 2))
    end
    lines_with_index
  end

  def keywords_dictionary
    dictionary = load_dictionary
    keywords = []
    dictionary.each do |entry|
      keywords << entry[0]
    end
    keywords
  end

  def grid_rows_words
    words = []
    grid.each do |row|
      words << row.join('')
    end
    words
  end

  def grid_rows_cols
  end


  def horizontal_search
    dictionary_words = keywords_dictionary
    row_words = grid_rows_words
    founded_words = []
    dictionary_words.each do |keyword|
      row_words.each do |row_word|
        if row_word.include?(keyword)
          founded_words << keyword if keyword != ''
        end
      end
    end
    return founded_words
  end

end

get '/' do
  if params[:n]
    @wordfinder = WordFinder.new
    @grid = @wordfinder.generate_grid(params[:n].to_i)
    slim :index
  else
    slim :form
  end
end
