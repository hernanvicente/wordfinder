Word Finder
=======

Using Ruby, generate an N x N grid, where N can be any number, and randomly populate the grid with letters (A-Z).

Using the provided dictionary file find all:

Horizontal words from left to right in your grid
Horizontal words from right to left in your grid
Vertical words from top to bottom in your grid
Vertical words from bottom to top in your grid
Diagonal words from left to right in your grid
Diagonal words from right to left in your grid

If possible, provide unit tests.

Please use all best practices that you deem relevant. Comment your code.

______________________________________________________________________________

This wordfinder was implemented to find and share quickly with my
recruiter the results of this challenge.

* grid is N * N
* identify the words in the grid and then search in the dictionary
* If possible, provide unit tests. (I don't have too many practice with
  TDD, I'll cover the test later)

Made with ruby, sinatra and foundation/pure-css
